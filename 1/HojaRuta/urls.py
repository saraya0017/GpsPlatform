#URLS HOJA RUTA
from django.urls import path
from HojaRuta import views

urlpatterns = [
    
    path('see_alls_records_roadmap/', views.see_alls_records_roadmap, name='see_all_record_roadmap'),
    path('update_alls_records_roadmap/', views.update_alls_records_roadmap, name='update_all_record_roadmap'),
    path('delete_alls_records_roadmap/', views.delete_alls_records_roadmap, name='delete_all_record_roadmap'),

    path('create_programa/', views.create_programs, name='create_programa'),
    path('update_programa/<id>/', views.update_programs, name='update_programa'),
    path('delete_programa/<id>/', views.delete_programs, name='delete_programa'),

    path('create_entry_roadmap/', views.create_entry_roadmap, name='create_entry_roadmap'),
    path('update_entry_roadmap/<id>/', views.update_entry_roadmap, name='update_entry_roadmap'),
    path('delete_entry_roadmap/<id>/', views.delete_entry_roadmap, name='delete_entry_roadmap'),

    path('create_cold_roadmap/', views.create_cold_roadmap, name='create_cold_roadmap'),
    path('update_cold_roadmap/<id>/', views.update_cold_roadmap, name='update_cold_roadmap'),
    path('delete_cold_roadmap/<id>/', views.delete_cold_roadmap, name='delete_cold_roadmap'),

    path('create_sag_roadmap/', views.create_sag_roadmap, name='create_sag_roadmap'),
    path('update_sag_roadmapa/<id>/', views.update_sag_roadmapa, name='update_sag_roadmapa'),
    path('delete_sag_roadmapa/<id>/', views.delete_sag_roadmapa, name='delete_sag_roadmapa'),

]

