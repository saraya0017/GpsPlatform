from django.contrib import admin
from HojaRuta.models import ProgramaEmbarqueModels,EntryRoadMapModels


class EntryRoadMapAdmin(admin.ModelAdmin):
    list_display = ("date", "hora_llegada", "chofer")
    search_fields = ("date", "hora_llegada", "chofer")
    list_filter = ("date",)
    date_hierarchy = ("date")
    readonly_fields = ('created_at', 'updated_at')


class ProgramaEmbarqueAdmin(admin.ModelAdmin):
    list_display = ("exportadora", "date_program", "especie","n_instructivo", "porteria_active","porteria_active","frio_active")
    search_fields = ("exportadora", "date_program", "especie","n_instructivo")
    list_filter = ("date_program","exportadora")
    date_hierarchy = ("date_program")
    readonly_fields = ('created_at', 'updated_at')


admin.site.register(EntryRoadMapModels, EntryRoadMapAdmin)
admin.site.register(ProgramaEmbarqueModels, ProgramaEmbarqueAdmin)
