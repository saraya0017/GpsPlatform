from django import forms
from HojaRuta.models import Exportadoras, Productores, Especie, Variedad, Camion, ProgramaEmbarqueModels,EntryRoadMapModels
from django.forms import ModelForm


class ProgramaEmbarqueForm(ModelForm):

    class Meta:
        model = ProgramaEmbarqueModels
        fields = [
            'exportadora',
            'date_program',
            'hora_planta',
            'hora_carpa',
            'hora_termino',
            'hora_puerto',
            'tipo_via',
            'tipo_transporte',
            'especie',
            'n_instructivo',
            'sps',
            'dus',
            'pto_embarque',
            'pais_destino',
            'etiqueta',
            'n_pallets',
            'tipo_inspeccion',
            'observaciones',
            'porteria_active',
            'frio_active',
            'sag_active',
            
        ] 
              
        widgets = {
            
            'date_program': forms.DateInput(attrs={"class":"form-control", "id":"example-date", "type":"date", "name":"date"}),
            'exportadora': forms.Select(attrs={"class": "form-control", "name": "exportadora", }),
            'hora_planta': forms.TimeInput(attrs={"class": "form-control","type":"time", "name":"time-planta"}),
            'hora_carpa': forms.TimeInput(attrs={"class": "form-control","type":"time", "name":"time-planta"}),
            'hora_termino': forms.TimeInput(attrs={"class": "form-control","type":"time", "name":"time-planta"}),
            'hora_puerto': forms.TimeInput(attrs={"class": "form-control","type":"time", "name":"time-planta"}),
            'tipo_via': forms.Select(attrs={"class": "form-select","type":"time", "name":"time-planta"}),
            'tipo_transporte': forms.Select(attrs={"class": "form-select"}),
            'especie': forms.Select(attrs={"class": "form-select"}),
            'n_instructivo': forms.TextInput(attrs={"class": "form-control"}),
            'sps': forms.TextInput(attrs={"class": "form-control"}),
            'dus': forms.TextInput(attrs={"class": "form-control"}),
            'pto_embarque': forms.Select(attrs={"class": "form-select"}),
            'pais_destino': forms.Select(attrs={"class": "form-select"}),
            'etiqueta': forms.Select(attrs={"class": "form-select"}),
            'n_pallets': forms.TextInput(attrs={"class": "form-control"}),
            'tipo_inspeccion': forms.Select(attrs={"class": "form-select"}),
            'observaciones': forms.Textarea(attrs={"class": "form-control","rows":"5"}),
            'porteria_active': forms.CheckboxInput(),
            'frio_active': forms.CheckboxInput(),
            'sag_active': forms.CheckboxInput(),
        }



class EntryRoadMapForm(ModelForm):

    class Meta:
        model = EntryRoadMapModels
        fields = [
            'programs',
            'date',
            'hora_llegada',
            'chofer',
            'observaciones',            
        ]  
        labels = {
            'programs' : "Escoger Programa",
            'date' : "Fecha",
            'hora_llegada' : "Hora de Llegada",
            'chofer' : "Chofer",
            'observaciones' : "Observaciones",     
        }     
        widgets = {
            'programs': forms.Select(attrs={"class": "form-control", "name": "chofer", }),
            'date': forms.DateInput(attrs={"class":"form-control date", "id":"example-date", "type":"date", "name":"date","data-provide":"typeahead"}),
            'chofer': forms.Select(attrs={"class": "form-control", "name": "chofer", }),
            'hora_llegada': forms.TimeInput(attrs={"class": "form-control","type":"time", "name":"time-planta"}),            
            'observaciones': forms.Textarea(attrs={"class": "form-control","rows":"3"}),
            
        }
