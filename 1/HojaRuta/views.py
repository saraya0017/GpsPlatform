from django.shortcuts import render, redirect, get_object_or_404
from .models import ProgramaEmbarqueModels, EntryRoadMapModels
from .forms import ProgramaEmbarqueForm, EntryRoadMapForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect




########################## PROGRAMA ############################
########################## PROGRAMA ############################
@csrf_protect
@login_required
def create_programs(request):
    registros = ProgramaEmbarqueModels.objects.all() #Ver inforamcion en tabla
    form = ProgramaEmbarqueForm(request.POST) 
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        return redirect('create_programa')

    return render(request, "programs/create.html", {"registros": registros, "form": ProgramaEmbarqueForm})

@login_required
def update_programs(request, id):
    registros = get_object_or_404(ProgramaEmbarqueModels, id=id) 
    
    data = {
        'form': ProgramaEmbarqueForm(instance=registros)
    }
    if request.method == "POST":      
        form = ProgramaEmbarqueForm(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            return redirect('create_programa')
        data["form"] = form   

    return render(request, "programs/update.html", data)

@login_required
def delete_programs(request,id):
    registros = ProgramaEmbarqueModels.objects.get(id=id)
    registros.delete()   
    return redirect('create_programa')



########################## HOJA FRIO ############################
########################## HOJA FRIO ############################

@login_required
def create_cold_roadmap(request):
    registros = ProgramaEmbarqueModels.objects.all() #Ver inforamcion en tabla
    form = ProgramaEmbarqueForm(request.POST) 
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        return redirect('create_cold_roadmap')

    return render(request, "cold_roadmap/create.html", {"registros": registros, "form": ProgramaEmbarqueForm})

@login_required
def update_cold_roadmap(request, id):
    registros = get_object_or_404(ProgramaEmbarqueModels, id=id) 
    
    data = {
        'form': ProgramaEmbarqueForm(instance=registros)
    }
    if request.method == "POST":      
        form = ProgramaEmbarqueForm(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            return redirect('create_cold_roadmap')
        data["form"] = form   

    return render(request, "cold_roadmap/update.html", data)

@login_required
def delete_cold_roadmap(request,id):
    registros = ProgramaEmbarqueModels.objects.get(id=id)
    registros.delete()   
    return redirect('create_cold_roadmap')


########################## HOJA SAG ############################
########################## HOJA SAG ############################

@login_required
def create_sag_roadmap(request):
    registros = ProgramaEmbarqueModels.objects.all() #Ver inforamcion en tabla
    form = ProgramaEmbarqueForm(request.POST) 
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        return redirect('create_sag_roadmap')

    return render(request, "sag_roadmap/create.html", {"registros": registros, "form": ProgramaEmbarqueForm})

@login_required
def update_sag_roadmapa(request, id):
    registros = get_object_or_404(ProgramaEmbarqueModels, id=id) 
    
    data = {
        'form': ProgramaEmbarqueForm(instance=registros)
    }
    if request.method == "POST":      
        form = ProgramaEmbarqueForm(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            return redirect('create_sag_roadmap')
        data["form"] = form   

    return render(request, "sag_roadmap/update.html", data)

@login_required
def delete_sag_roadmapa(request,id):
    registros = ProgramaEmbarqueModels.objects.get(id=id)
    registros.delete()   
    return redirect('create_sag_roadmap')





########################## PORTERIA ############################
########################## PORTERIA ############################

@login_required
def create_entry_roadmap(request):
    registros = EntryRoadMapModels.objects.all() #Ver inforamcion en tabla
    form = EntryRoadMapForm(request.POST) 
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        return redirect('create_entry_roadmap')

    return render(request, "entry_roadmap/create.html", {"registros": registros, "form": EntryRoadMapForm})

@login_required
def update_entry_roadmap(request, id):
    registros = get_object_or_404(ProgramaEmbarqueModels, id=id)     
    data = {
        'form': ProgramaEmbarqueForm(instance=registros)
    }
    if request.method == "POST":      
        form = ProgramaEmbarqueForm(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            return redirect('create_sag_roadmap')
        data["form"] = form   

    return render(request, "sag_roadmap/update.html", data)

@login_required
def delete_entry_roadmap(request,id):
    registros = ProgramaEmbarqueModels.objects.get(id=id)
    registros.delete()   
    return redirect('create_sag_roadmap')



########################## TODOS LOS REGISTROS ############################
########################## TODOS LOS REGISTROS ############################

@login_required
def see_alls_records_roadmap(request):
    registros = ProgramaEmbarqueModels.objects.all() #Ver inforamcion en tabla
    form = ProgramaEmbarqueForm(request.POST) 
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        return redirect('create_sag_roadmap')

    return render(request, "sag_roadmap/create.html", {"registros": registros, "form": ProgramaEmbarqueForm})

@login_required
def update_alls_records_roadmap(request, id):
    registros = get_object_or_404(ProgramaEmbarqueModels, id=id) 
    
    data = {
        'form': ProgramaEmbarqueForm(instance=registros)
    }
    if request.method == "POST":      
        form = ProgramaEmbarqueForm(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            return redirect('create_sag_roadmap')
        data["form"] = form   

    return render(request, "sag_roadmap/update.html", data)

@login_required
def delete_alls_records_roadmap(request,id):
    registros = ProgramaEmbarqueModels.objects.get(id=id)
    registros.delete()   
    return redirect('create_sag_roadmap')