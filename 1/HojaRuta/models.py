from django.db import models
from TabDim.models import Exportadoras, Productores, Variedad, Camion, Especie



TIPO_VIA = (
    ('A', 'Aereo'),
    ('M', 'Maritimo'),
    ('T', 'Terrestre'),
    ('M/T', 'Maritimo/Terrestre'),
)

TIPO_TRANSPORTE = (
    ('T1', 'Transporte 1'),
    ('T2', 'Transporte 2'),
    ('T3', 'Transporte 3'),
    ('T4', 'Transporte 4'),
    )

PTO_EMBARQUE = (
    ('Em1', 'Embarque 1'),
    ('Em2', 'Embarque 2'),
    ('Em3', 'Embarque 3'),
    ('Em4', 'Embarque 4'),
    )

ETIQUETA = (
    ('Et1', 'Etiqueta1'),
    ('Et2', 'Etiqueta2'),
    ('Et3', 'Etiqueta3'),
    ('Et4', 'Etiqueta4'),
    )

PAIS_DESTINO = (
    ('CH', 'China'),
    ('EEUU', 'Estados Unidos'),
    ('CAN', 'Canada'),
    ('BR', 'Brasil'),
    )
TIPO_INSPECCION = (
    ('USDA', 'USDA'),
    ('SAG', 'SAG'),
    ('ORIGEN', 'ORIGEN'),
    )


class ProgramaEmbarqueModels(models.Model):    
    exportadora = models.ForeignKey(Exportadoras, on_delete=models.CASCADE)
    date_program = models.DateField()
    hora_planta = models.TimeField(null=True)#, required=True, unique=True)
    hora_carpa = models.TimeField(null=True)#, required=True, unique=True)
    hora_termino = models.TimeField(null=True)#, required=True, unique=True)
    hora_puerto = models.TimeField(null=True)#, required=True, unique=True)
    tipo_via = models.CharField(max_length=55, choices=TIPO_VIA, default='Maritimo')
    tipo_transporte = models.CharField(max_length=55, choices=TIPO_TRANSPORTE, default='Contenedor')
    especie = models.ForeignKey(Especie, on_delete=models.CASCADE)    
    n_instructivo = models.PositiveIntegerField(verbose_name='N° Instructivo')
    sps = models.CharField(max_length=55,blank=True, null=True)
    dus = models.CharField(max_length=100,blank=True, null=True)
    pto_embarque = models.CharField(max_length=100, choices=PTO_EMBARQUE, default='Valparaiso',blank=True, null=True)
    pais_destino = models.CharField(max_length=100, choices=PAIS_DESTINO, default='China',blank=True, null=True)
    etiqueta = models.CharField(max_length=100, choices=ETIQUETA, default='China',blank=True, null=True)
    n_pallets = models.PositiveIntegerField()
    tipo_inspeccion = models.CharField(max_length=100, choices=TIPO_INSPECCION, default='Valparaiso',blank=True, null=True)
    observaciones = models.TextField(max_length=1000, blank=True,null=True)
    porteria_active = models.BooleanField()
    frio_active = models.BooleanField()
    sag_active = models.BooleanField()    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Programa de Embarque'
        verbose_name_plural = 'Programas de Embarques'

    def __str__(self):
        return 'SPS:{},  DUS:{},  Llegada:{},  Exportadora:{}'.format(self.sps, self.dus, self.hora_planta, self.exportadora)


class EntryRoadMapModels(models.Model):
    programs = models.ForeignKey(ProgramaEmbarqueModels, on_delete=models.CASCADE)
    date = models.DateField()
    hora_llegada = models.TimeField(null=True)#, required=True, unique=True)
    chofer = models.ForeignKey(Camion, on_delete=models.CASCADE)        
    observaciones = models.TextField(max_length=550, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Hoja de Ruta Portería'
        verbose_name_plural = 'Hojas de Ruta Portería'
    
    def __str__(self):
        return '{} {}'.format(self.date, self.chofer)