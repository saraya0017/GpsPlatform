from django import forms
from .models import Exportadoras, Productores, Especie, Variedad, Camion, Envases
from django.forms import ModelForm


class formExportadoras(ModelForm):
   
    class Meta:
        model = Exportadoras
        fields = ['Nombre', 'Direccion', 'Telefono', 'Email']
        labels = {
            'Nombre': 'Nombre Exportadora',
            'Direccion': 'Dirección',
            'Telefono': 'Telefono',
            'Email': 'Correo Electrónico',            
        }
        widgets = {
            'Nombre': forms.TextInput(attrs={"class": "form-control","name": "nombre","type": "text"}),
            'Direccion': forms.TextInput(attrs={"class": "form-control","name": "direccion",}),
            'Telefono': forms.TextInput(attrs={"class": "form-control","name": "telefono","data-toggle": "input-mask", "data-mask-format": "(+56) 0 0000 0000",}),
            'Email': forms.EmailInput(attrs={"class": "form-control"}),            
        }


class formProductores(ModelForm):
    
    class Meta:
        model = Productores
        fields = ['Nombre','CSG', 'Direccion', 'Telefono', 'Email','Exportadora']
        labels = {
            'Nombre': 'Nombre Productor',
            'CSG': 'Codigo SAG (CSG)',
            'Direccion': 'Dirección',
            'Telefono': 'Telefono',
            'Email': 'Correo Electrónico',
            'Exportadora': 'Exportadora',             
        }
        widgets = {
            'Nombre': forms.TextInput(attrs={"class": "form-control","name": "nombre",}),
            'CSG': forms.TextInput(attrs={"class": "form-control","name": "CSG",}),
            'Direccion': forms.TextInput(attrs={"class": "form-control","name": "direccion",}),
            'Telefono': forms.TextInput(attrs={"class": "form-control","name": "telefono","data-toggle": "input-mask", "data-mask-format": "(+56) 0 0000 0000",}),
            'Email': forms.EmailInput(attrs={"class": "form-control"}),  
            'Exportadora': forms.Select(attrs={"class": "form-control","name": "exportadora",}),          
        }



class formEspecie(ModelForm):
    
    class Meta:
        model = Especie
        fields = ['Nombre']
        labels = {
            'Nombre': 'Nombre Especie',           
        }
        widgets = {
            'Nombre': forms.TextInput(attrs={"class": "form-control","name": "especie"}),           
        }

class formVariedad(ModelForm):
    
    class Meta:
        model = Variedad
        fields = ['Nombre', 'Especie']
        labels = {
            'Nombre': 'Nombre Variedad',
            'Especie': 'Nombre Especie',
            
        }
        widgets = {
            'Nombre': forms.TextInput(attrs={"class": "form-control","name": "variedad"}),
            'Especie': forms.Select(attrs={"class": "form-control","name": "especie"}),
        }

class formEnvases(ModelForm):
    
    class Meta:
        model = Envases
        fields = ['Codigo', 'Nombre','Tipo']
        labels = {
            'Codigo': 'Codigo',
            'Nombre': 'Nombre Envase',
            'Tipo': 'Tipo Envase',
        }
        widgets = {
            'Codigo': forms.TextInput(attrs={"class": "form-control","name": "codigo"}),
            'Nombre': forms.TextInput(attrs={"class": "form-control","name": "nombre"}),
            'Tipo': forms.TextInput(attrs={"class": "form-control","name": "tipo"}),
        }


class formCamion(ModelForm):
    
    class Meta:
        model = Camion
        fields = ['Nombres', 'Apellidos', 'Rut', 'Patente1',
                  'Patente2', 'Telefono', 'Exportadora']
        labels = {
            'Nombres': 'Nombres (Chofer)',
            'Apellidos': 'Apellidos (Chofer)',
            'Rut': 'Rut (Dni)',
            'Patente1': 'Patente Camión',
            'Patente2': 'Patente Carro',
            'Telefono': 'Telefono Chofer',
            'Exportadora': 'Nombre Exportadora',
        }
        widgets = {
            'Nombres': forms.TextInput(attrs={"class": "form-control"}),
            'Apellidos': forms.TextInput(attrs={"class": "form-control"}),
            'Rut': forms.TextInput(attrs={"class": "form-control","data-toggle": "input-mask","data-mask-format": "00.000.000-A",}),
            'Patente1': forms.TextInput(attrs={"class": "form-control","data-toggle": "input-mask", "data-mask-format": "AA-AA-00",}),
            'Patente2': forms.TextInput(attrs={"class": "form-control","data-toggle": "input-mask", "data-mask-format": "AA-AA-00",}),
            'Telefono': forms.TextInput(attrs={"class": "form-control","data-toggle": "input-mask", "data-mask-format": "(+56) 0 0000 0000",}),
            'Exportadora': forms.Select(attrs={"class": "form-control"}),
            
        }
