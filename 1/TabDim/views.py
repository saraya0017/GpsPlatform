
from django.shortcuts import render, redirect, get_object_or_404
from .forms import formCamion, formEspecie, formVariedad, formExportadoras, formProductores, formEnvases
from django.contrib.auth.decorators import login_required
from .models import Camion, Especie, Exportadoras, Productores, Variedad, Envases
from django.contrib import messages


########################## EXPORTADORA ############################
########################## EXPORTADORA ############################
@login_required
def update_exportadora(request, id):
    registros = get_object_or_404(Exportadoras, id=id) 
    
    data = {
        'form': formExportadoras(instance=registros)
    }
    if request.method == "POST":      
        form = formExportadoras(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Exportadora Editada Exitosamente!')
            return redirect('all_exportador')
        data["form"] = form   

    return render(request, "exportadoras/update.html", data)
@login_required
def delete_exportadora(request,id):
    registros = Exportadoras.objects.get(id=id)
    registros.delete()  
    messages.success(request, '!Exportadora Eliminada Exitosamente!') 
    return redirect('all_exportador')

@login_required
def exportadoras(request):
    registros = Exportadoras.objects.all()
    form = formExportadoras(request.POST)
    if form.is_valid():
        new_exportador = form.save(commit=False)
        new_exportador.user = request.user
        new_exportador.save()
        messages.success(request, '!Exportadora Creada Exitosamente!')
        return redirect('all_exportador')

    return render(request, "exportadoras/all.html", {"registros": registros, "form": formExportadoras})



########################## PRODUCTORES ############################
########################## PRODUCTORES ############################

def update_productores(request, id):
    registros = get_object_or_404(Productores, id=id) 
    
    data = {
        'form': formProductores(instance=registros)
    }
    if request.method == "POST":      
        form = formProductores(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Productor Editado Exitosamente!')
            return redirect('all_productor')
        data["form"] = form   

    return render(request, "productores/update.html", data)

def delete_productores(request, id):
    registros = Productores.objects.get(id=id)
    registros.delete()
    messages.success(request, '!Productor Eliminado Exitosamente!')
    return redirect('all_productor')

@login_required
def productores(request):
    registros = Productores.objects.all()
    form = formProductores(request.POST)
    if form.is_valid():
        new_productor = form.save(commit=False)
        new_productor.user = request.user
        new_productor.save()
        messages.success(request, '!Productor Creado Exitosamente!')
        return redirect('all_productor')

    return render(request, "productores/all.html", {"registros": registros, "form": formProductores})



########################## ESPECIES ############################
########################## ESPECIES ############################

def update_especies(request, id):
    registros = get_object_or_404(Especie, id=id) 
    
    data = {
        'form': formEspecie(instance=registros)
    }
    if request.method == "POST":      
        form = formEspecie(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Especie Editada Exitosamente!')
            return redirect('all_especie')
        data["form"] = form   

    return render(request, "especies/update.html", data)

def delete_especies(request, id):
    registros = Especie.objects.get(id=id)
    registros.delete()
    messages.success(request, '!Especie Eliminada Exitosamente!')
    return redirect('all_especie')

@login_required
def especies(request):
    registros = Especie.objects.all()
    form = formEspecie(request.POST)
    if form.is_valid():
        new_especie = form.save(commit=False)
        new_especie.user = request.user
        new_especie.save()
        messages.success(request, '!Especie Creada Exitosamente!')
        return redirect('all_especie')

    return render(request, "especies/all.html", {"registros": registros, "form": formEspecie}) 


    

########################## ESPECIES ############################
########################## ESPECIES ############################

def update_variedades(request, id):
    registros = get_object_or_404(Variedad, id=id) 
    
    data = {
        'form': formVariedad(instance=registros)
    }
    if request.method == "POST":      
        form = formVariedad(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Variedad Editada Exitosamente!')
            return redirect('all_variedad')
        data["form"] = form   

    return render(request, "variedades/update.html", data)


def delete_variedades(request, id):
    registros = Variedad.objects.get(id=id)
    registros.delete()
    messages.success(request, '!Variedad Eliminada Exitosamente!')
    return redirect('all_variedad')

@login_required
def variedades(request):
    registros = Variedad.objects.all()
    form = formVariedad(request.POST)
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        new_variedad.save()
        messages.success(request, '!Variedad Creada Exitosamente!')
        return redirect('all_variedad')

    return render(request, "variedades/all.html", {"registros": registros, "form": formVariedad}) 

    
########################## CAMIONES ############################
########################## CAMIONES ############################

def update_camiones(request, id):
    registros = get_object_or_404(Camion, id=id) 
    
    data = {
        'form': formCamion(instance=registros)
    }
    if request.method == "POST":      
        form = formCamion(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Camión Editado Exitosamente!')
            return redirect('all_camion')
        else:
            messages.success(request, '!El Camión No se ha podido registrar Exitosamente! - Revisa los campos completados')
            data["form"] = form  

    return render(request, "camiones/update.html", data)

def delete_camiones(request, id):
    registros = Camion.objects.get(id=id)
    registros.delete()
    messages.success(request, '!Camión Eliminado Exitosamente!')
    return redirect('all_camion')

@login_required
def camiones(request):
    registros = Camion.objects.all()
    
    form = formCamion(request.POST)
    if form.is_valid():
        new_variedad = form.save(commit=False)
        new_variedad.user = request.user
        messages.success(request, '!Camión Creado Exitosamente!')
        new_variedad.save()        
        return redirect('all_camion')      
    
    
    return render(request, "camiones/all.html", {"registros": registros, "form": formCamion})
    
    
    



########################## ENVASES  ############################
########################## ENVASES ############################


def update_envases(request, id):
    registros = get_object_or_404(Envases, id=id) 
    
    data = {
        'form': formEnvases(instance=registros)
    }
    if request.method == "POST":      
        form = formEnvases(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Envase Editado Exitosamente!')
            return redirect('all_envase')
        data["form"] = form   

    return render(request, "envases/update.html", data)    


def delete_envases(request, id):
    registros = Envases.objects.get(id=id)
    registros.delete()
    messages.success(request, '!Envase Eliminado Exitosamente!')
    return redirect('all_envase')


@login_required
def envases(request):
    registros = Envases.objects.all()
    form = formEnvases(request.POST)
    if form.is_valid():
        new_envase = form.save(commit=False)
        new_envase.user = request.user
        new_envase.save()
        messages.success(request, '!Envase Creado Exitosamente!')
        return redirect('all_envase')

    return render(request, "envases/all.html", {"registros": registros, "form": formEnvases})










        
        




