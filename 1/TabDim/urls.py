#URLS Principal
from django.urls import path
from TabDim import views

urlpatterns = [ 

    path('all_variedades/', views.variedades, name='all_variedad'),
    path('all_especies/', views.especies, name='all_especie'),
    path('all_envases/', views.envases, name='all_envase'),
    path('all_productores/', views.productores, name='all_productor'),
    path('all_exportadores/', views.exportadoras, name='all_exportador'),
    path('all_camiones/', views.camiones, name='all_camion'),

    path('delete_exportadora/<id>/', views.delete_exportadora, name='delete_exportadora'),
    path('delete_productores/<id>/', views.delete_productores, name='delete_productores'),
    path('delete_especies/<id>/', views.delete_especies, name='delete_especies'),
    path('delete_variedades/<id>/', views.delete_variedades, name='delete_variedades'),
    path('delete_camiones/<id>/', views.delete_camiones, name='delete_camiones'),
    path('delete_envases/<id>/', views.delete_envases, name='delete_envases'),

    path('update_exportadora/<id>/', views.update_exportadora, name='update_exportadora'),
    path('update_productores/<id>/', views.update_productores, name='update_productores'),
    path('update_especies/<id>/', views.update_especies, name='update_especies'),
    path('update_variedades/<id>/', views.update_variedades, name='update_variedades'),
    path('update_camiones/<id>/', views.update_camiones, name='update_camiones'),
    path('update_envases/<id>/', views.update_envases, name='update_envases'),
    
]
