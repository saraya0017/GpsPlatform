from django.db import models


class Exportadoras(models.Model):
    Nombre = models.CharField(
        max_length=70, verbose_name='Nombre Exportadora', unique=True)
    Direccion = models.CharField(
        max_length=70, verbose_name='Dirección')
    Telefono = models.CharField(max_length=20, null=True)
    Email = models.EmailField(max_length=70,
                              verbose_name='Correo Electrónico')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Exportadora'
        verbose_name_plural = 'Exportadoras'

    def __str__(self):
        return '{}'.format(self.Nombre)


class Productores(models.Model):
    Nombre = models.CharField(max_length=70, unique=True)
    CSG = models.IntegerField(null=False, unique=True)
    Direccion = models.CharField(max_length=70, null=True)
    Telefono = models.CharField(max_length=20, null=True)
    Email = models.EmailField(max_length=70,
                              verbose_name='Correo Electrónico')
    Exportadora = models.ForeignKey(
        Exportadoras, on_delete=models.CASCADE, verbose_name='Exportadora')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Productor'
        verbose_name_plural = 'Productores'

    def __str__(self):
        return self.Nombre


class Especie(models.Model):
    Nombre = models.CharField(max_length=70, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Especie'
        verbose_name_plural = 'Especies'

    def __str__(self):
        return self.Nombre

class Envases(models.Model):
    Codigo = models.CharField(max_length=70, unique=True)
    Nombre = models.CharField(max_length=70, unique=True)
    Tipo = models.CharField(max_length=70, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Envase'
        verbose_name_plural = 'Envases'

    def __str__(self):
        return '{} {}'.format(self.Nombre,self.Tipo)




class Variedad(models.Model):
    Nombre = models.CharField(max_length=70, unique=True)
    Especie = models.ForeignKey(Especie, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Variedad'
        verbose_name_plural = 'Variedades'

    def __str__(self):
        return '{}'.format(self.Nombre)


class Camion(models.Model):
    Nombres = models.CharField(max_length=100)
    Apellidos = models.CharField(max_length=100)
    Rut = models.CharField(max_length=13, unique=True)
    Patente1 = models.CharField(max_length=10, null=True, unique=True)
    Patente2 = models.CharField(max_length=10, null=True, unique=True)
    Telefono = models.CharField(max_length=20, null=True)
    Exportadora = models.ForeignKey(
        Exportadoras, on_delete=models.CASCADE, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Camión'
        verbose_name_plural = 'Camiones'

    def __str__(self):
        return '{}/{}'.format(self.Patente1,self.Patente2)
