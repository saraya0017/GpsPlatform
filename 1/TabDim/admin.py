from django.contrib import admin
from TabDim.models import Exportadoras, Productores, Especie, Variedad, Camion, Envases
#from . forms import formCamion

class ExportadoresAdmin(admin.ModelAdmin):
    list_display = ("Nombre", "Telefono", "Email")
    search_fields = ("Nombre", "Email")
    list_filter = ("Nombre", "Email",)
    readonly_fields = ('created_at', 'updated_at')


class ProductoresAdmin(admin.ModelAdmin):
    list_display = ("Nombre", "CSG", "Telefono", "Email")
    search_fields = ("Nombre", "CSG")
    list_filter = ("Nombre", "Email",)
    readonly_fields = ('created_at', 'updated_at')


class CamionAdmin(admin.ModelAdmin):
    list_display = ("Nombres", "Rut", "Patente1", "Patente2")
    search_fields = ("Nombres", "Patente1")
    list_filter = ("Nombres", "Rut",)
    readonly_fields = ('created_at', 'updated_at')
    

class VariedadAdmin(admin.ModelAdmin):
    list_display = ("Nombre", "Especie")
    search_fields = ("Nombre", "Especie")
    list_filter = ("Especie",)
    readonly_fields = ('created_at', 'updated_at')

class EnvaseAdmin(admin.ModelAdmin):
    list_display = ("Codigo", "Nombre", "Tipo")
    search_fields = ("Nombre", "Tipo")
    list_filter = ("Tipo",)
    readonly_fields = ('created_at', 'updated_at')


admin.site.register(Exportadoras, ExportadoresAdmin)
admin.site.register(Productores, ProductoresAdmin)
admin.site.register(Especie)
admin.site.register(Variedad, VariedadAdmin)
admin.site.register(Camion, CamionAdmin)
admin.site.register(Envases, EnvaseAdmin)
