from django.apps import AppConfig


class TabdimConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TabDim'
