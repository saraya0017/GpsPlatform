from django.contrib import admin

from PostCosecha.models import FormCerezaModels


class PrincipalAdmin(admin.ModelAdmin):
    list_display = ("Lote", "Nguia", "Exportadora", "Productor" , "Fecha_Recepcion")
    search_fields = ("Lote", "Fecha_Recepcion", "Nguia")
    list_filter = ("Exportadora", "Fecha_Recepcion", "Productor", "Variedad",)
    date_hierarchy = ("Fecha_Recepcion")
    readonly_fields = ('created_at', 'updated_at')


admin.site.register(FormCerezaModels, PrincipalAdmin)
