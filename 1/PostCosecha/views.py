from django.shortcuts import render, redirect, get_object_or_404
from PostCosecha.models import FormCerezaModels
from django.contrib.auth.decorators import login_required
from .forms import PrincipalForms
from django.contrib import messages



@login_required
def create_form_cereza(request):
    form = PrincipalForms(request.POST)
    if form.is_valid():
        new_principal = form.save(commit=False)
        new_principal.user = request.user
        new_principal.save() 
        messages.success(request, '!Registro de Cereza creado Exitosamente!')  
        return redirect('read_cerezas')

    return render(request, "cerezas/create.html", {"r": PrincipalForms})

def read_form_cereza(request):
    registros = FormCerezaModels.objects.all()
    return render(request, "cerezas/read.html", {"registros": registros})


@login_required
def update_form_cereza(request, Lote):
    registros = get_object_or_404(FormCerezaModels, Lote=Lote) 
    
    data = {
        'r': PrincipalForms(instance=registros)
    }
    if request.method == "POST":      
        form = PrincipalForms(data=request.POST, instance=registros)#, files=request.FILES)
        if form.is_valid():            
            form.save()
            messages.success(request, '!Registro de Cereza actualizado Exitosamente!')  
            return redirect('read_cerezas')
        data["form"] = form   

    return render(request, "cerezas/update.html", data)



@login_required
def delete_form_cereza(request,Lote):
    registros = FormCerezaModels.objects.get(Lote=Lote)
    registros.delete()   
    messages.success(request, '!Registro de Cereza eliminado Exitosamente!')  
    return redirect('read_cerezas')


