# Generated by Django 4.1.3 on 2022-11-14 12:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PostCosecha', '0002_formcerezamodels_promdark_formcerezamodels_promlight_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='formcerezamodels',
            name='PrecalibreTot',
            field=models.FloatField(default=0),
        ),
    ]
