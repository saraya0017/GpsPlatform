# URLS POSTCOSECHA
from django.urls import path
from PostCosecha import views


urlpatterns = [
    path('read_form_cereza/', views.read_form_cereza, name='read_cerezas'),
    path('create_form_cereza/', views.create_form_cereza, name='create_cerezas'),
    path('delete_form_cereza/<Lote>/', views.delete_form_cereza, name='delete_form_cereza'),
    path('update_form_cereza/<Lote>/', views.update_form_cereza, name='update_form_cereza'),

]
