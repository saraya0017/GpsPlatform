from django.urls import path, include
from Login import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    #App LOGIN    
    #path('', LoginView.as_view(template_name='login/signin.html'), name='login'),
    path('', views.signin, name='login'),
    path('logout/', LogoutView.as_view(template_name='login/logout.html'), name='logout'),
    path('signup/', views.signup, name='register'),
    path('confirm_email/', views.confirm_email, name='confirm_email'),
    path('recover-pass/', views.recover_pass, name='recoverpw'),  
    path('profile/', views.profile, name='profile'),
	path('profile/<str:username>/', views.profile, name='profile'),
]
