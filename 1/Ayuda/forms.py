from django import forms

class formHelp(forms.Form):
    asunto=forms.CharField(max_length=60)
    nombres = forms.CharField(max_length=70)
    apellidos = forms.CharField(max_length=70)
    telefono = forms.IntegerField()
    email=forms.EmailField(max_length=60)
    mensaje=forms.CharField(max_length=60)
    