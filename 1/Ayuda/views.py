from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
from django.views.decorators.csrf import csrf_protect



@csrf_protect
def contactanos(request):
    if request.method == "POST":

        asunto = request.POST["asunto"]
        nombres = request.POST["nombres"]
        apellidos = request.POST["apellidos"]
        telefono = request.POST["telefono"]
        email = request.POST["email"]
        mensaje = request.POST["mensaje"]

        enviar = asunto + " " + nombres + " " + apellidos + \
            " " + telefono + " " + email + " " + mensaje
        email_from = settings.EMAIL_HOST_USER

        recipient_list = ["sebastian.araya@greenps.cl"]

        send_mail(asunto, enviar, email_from, recipient_list)
        
        return render(request, "mensaje_enviado.html")

    

    return render(request, "form_contacto.html")
